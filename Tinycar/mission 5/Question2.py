panier = int(input("Veuillez entrer la taille du panier :"))
accessoires = []
prixHT = []
prixTTC = []

def calculer_TTC(prixHT):
    tva = 0.20 
    prixTTC = prixHT * (1 + tva) 
    return prixTTC

def mon_panier(panier):
    print("Veuillez entrer les noms et prix HT pour chaque accessoire :")
    for i in range(panier):
        nom = input(f"Nom de l'accessoire {i + 1} : ")
        prixHT_nom = float(input(f"Prix HT pour {nom} : "))
        accessoires.append(nom)
        prixHT.append(prixHT_nom)
        prixTTC.append(calculer_TTC(prixHT_nom))

mon_panier(panier)

print("Noms, prix HT, et prix TTC des accessoires du panier :")
for i in range(panier):
    print(f"{accessoires[i]} - Prix HT : {prixHT[i]} € - Prix TTC : {prixTTC[i]} €")

somme_HT = sum(prixHT)
somme_TTC = sum(prixTTC)
print("La somme totale des achats (HT) :" , somme_HT, "€")
print("La somme totale des achats (TTC) :", somme_TTC, "€")

minimum = prixHT[0]
for i in prixHT:
    if i < minimum:
        minimum = i
print("le prix(HT) le plus bas dans tous vous accessoires est",minimum,"€")

minimum_TTC = prixTTC[0]
for i in prixTTC:
    if i < minimum_TTC:
        minimum_TTC = i
print("le prix(TTC) le plus bas dans tous vous accessoires est",minimum_TTC,"€")

max = prixHT[0]
for i in prixHT:
    if i > max:
        max = i
print("le prix(HT) le plus haut dans tous vous accessoires est",max,"€")


max_TTC = prixTTC[0]
for i in prixTTC:
    if i > max_TTC:
        max_TTC = i
print("le prix(TTC) le plus haut dans tous vous accessoires est",max_TTC,"€")

print("le prix (HT) moyen de tous vous accessoires est",(sum(prixHT)/len(prixHT)),"€")

print("le prix (TTC) moyen de tous vous accessoires est",(sum(prixTTC)/len(prixHT)),"€")